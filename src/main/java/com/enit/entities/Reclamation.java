package com.enit.entities;

import javax.persistence.*;

@Entity
public class Reclamation {

    @Id
    @GeneratedValue
    private Integer id;
    private String titre;
    private String description;

    @ManyToOne
    @JoinColumn(name = "id_etudiant")
    private Etudiant etudiant;
    @ManyToOne
    @JoinColumn(name = "id_type_reclamation")
    private TypeReclamation typeReclamation;

    public Reclamation() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    public TypeReclamation getTypeReclamation() {
        return typeReclamation;
    }

    public void setTypeReclamation(TypeReclamation typeReclamation) {
        this.typeReclamation = typeReclamation;
    }
}
