package com.enit.service;

import com.enit.entities.Etudiant;
import com.enit.entities.Utilisateur;

import javax.ejb.Local;
import java.util.List;

@Local
public interface AuthentificationServiceLocal {

    Utilisateur authentifier(String login, String pwd);

    boolean existeEmail(String login);

    void sauvegarderUtilisateur(Utilisateur utilisateur);

    List<Etudiant> listerEtudiants();

}
