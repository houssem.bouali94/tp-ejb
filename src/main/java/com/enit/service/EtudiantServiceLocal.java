package com.enit.service;

import com.enit.entities.Etudiant;

import javax.ejb.Local;
import java.util.List;

@Local
public interface EtudiantServiceLocal {

    void ajouterEtudiant(Etudiant etudiant);

    void supprimerEtudiant(Etudiant etudiant);

    Etudiant chercherEtudiantParEmail(String email);

    List<Etudiant> listerEtudiants();
}
