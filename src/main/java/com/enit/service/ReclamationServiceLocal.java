package com.enit.service;

import com.enit.entities.Etudiant;
import com.enit.entities.Reclamation;
import com.enit.entities.TypeReclamation;

import javax.ejb.Local;
import java.util.List;

@Local
public interface ReclamationServiceLocal {

    void ajouterReclamation(Reclamation reclamation);

    void ajouterTypeReclamation(TypeReclamation typeReclamation);

    List<Reclamation> listerReclamations();

    List<TypeReclamation> listerTypeReclamations();

    boolean existeTypeReclalamtion(String type);

    TypeReclamation chercherTypeReclamationParType(String type);

    List<Reclamation> listerReclamationParEtudiant(Etudiant etudiant);
}
