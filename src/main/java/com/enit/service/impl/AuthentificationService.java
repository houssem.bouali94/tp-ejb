package com.enit.service.impl;

import com.enit.entities.Etudiant;
import com.enit.entities.Utilisateur;
import com.enit.service.AuthentificationServiceLocal;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class AuthentificationService implements AuthentificationServiceLocal {

    @PersistenceContext(name = "examenPU")
    private EntityManager em;

    @Override
    public Utilisateur authentifier(String login, String pwd) {
        return null;
    }

    @Override
    public boolean existeEmail(String login) {
        String queryStr = "SELECT * FROM ETUDIANT WHERE email like " + login;
        Query nativeQuery = em.createNativeQuery(queryStr, Etudiant.class);
        try {
            nativeQuery.getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    public void sauvegarderUtilisateur(Utilisateur utilisateur) {
        em.persist(utilisateur);
    }

    @Override
    public List<Etudiant> listerEtudiants() {
        String queryStr = "SELECT * FROM ETUDIANT";
        Query nativeQuery = em.createNativeQuery(queryStr, Etudiant.class);
        return nativeQuery.getResultList();
    }
}
