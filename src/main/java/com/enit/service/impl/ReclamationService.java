package com.enit.service.impl;

import com.enit.entities.Etudiant;
import com.enit.entities.Reclamation;
import com.enit.entities.TypeReclamation;
import com.enit.service.ReclamationServiceLocal;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class ReclamationService implements ReclamationServiceLocal {

    @PersistenceContext(name = "examenPU")
    private EntityManager em;

    @Override
    public void ajouterReclamation(Reclamation reclamation) {
        em.persist(reclamation);
    }

    @Override
    public void ajouterTypeReclamation(TypeReclamation typeReclamation) {
        em.persist(typeReclamation);
    }

    @Override
    public List<Reclamation> listerReclamations() {
        String queryString = "SELECT * FROM RECLAMATION";
        Query query = em.createNativeQuery(queryString, Reclamation.class);
        return (List<Reclamation>) query.getResultList();
    }

    @Override
    public List<TypeReclamation> listerTypeReclamations() {
        String queryString = "SELECT * FROM TYPE_RECLAMATION";
        Query query = em.createNativeQuery(queryString, Reclamation.class);
        return (List<TypeReclamation>) query.getResultList();    }

    @Override
    public boolean existeTypeReclalamtion(String type) {
        String queryString = "SELECT count(tr.id) FROM TYPE_RECLAMATION tr WHERE tr.type = " + type;
        Query query = em.createNativeQuery(queryString, TypeReclamation.class);
        int firstResult = query.getFirstResult();
        return firstResult > 0;
    }

    @Override
    public TypeReclamation chercherTypeReclamationParType(String type) {
        String queryString = "SELECT count(tr.id) FROM TYPE_RECLAMATION tr WHERE tr.type = " + type;
        Query query = em.createNativeQuery(queryString, TypeReclamation.class);
        return (TypeReclamation) query.getSingleResult();
    }

    @Override
    public List<Reclamation> listerReclamationParEtudiant(Etudiant etudiant) {
        String queryStr = "SELECT r.* FROM RECLAMATION r WHERE r.id_etudiant = " + etudiant.getId();
        Query query= em.createNativeQuery(queryStr, Reclamation.class);
        return query.getResultList();
    }
}
