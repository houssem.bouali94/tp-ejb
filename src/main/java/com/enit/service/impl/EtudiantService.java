package com.enit.service.impl;

import com.enit.entities.Etudiant;
import com.enit.service.EtudiantServiceLocal;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class EtudiantService implements EtudiantServiceLocal {

    @PersistenceContext(name = "examenPU")
    private EntityManager em;

    @Override
    public void ajouterEtudiant(Etudiant etudiant) {
        if (etudiant != null) {
            em.persist(etudiant);
        }
    }

    @Override
    public void supprimerEtudiant(Etudiant etudiant) {
        if (etudiant != null) {
            em.remove(etudiant);
        }
    }

    @Override
    public Etudiant chercherEtudiantParEmail(String email) {
        String selectQuery = "SELECT e.* FROM ETUDIANT e WHERE e.email = " + email;
        Query query = em.createNativeQuery(selectQuery, Etudiant.class);
        return (Etudiant) query.getSingleResult();
    }

    @Override
    public List<Etudiant> listerEtudiants() {
        String selectQuery = "SELECT * FROM ETUDIANT";
        Query query = em.createNativeQuery(selectQuery, Etudiant.class);
        return (List<Etudiant>) query.getResultList();
    }
}
